﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManejaAnimacion : MonoBehaviour {

    public Animator[] anim;
    bool frente = false;
	// Use this for initialization
	void Start () {
        for (byte i = 0; i < anim.Length; i++)
        {
            anim[i].SetTrigger("Corre");
        }
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.A))
        {
            for (byte i = 0; i < anim.Length; i++)
            {
                anim[i].SetTrigger("Corre");
            }
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            halfTurn();
        }
    }

    public void halfTurn()
    {
        for (byte i = 0; i < anim.Length; i++)
        {
            anim[i].SetTrigger("Gira");
            StartCoroutine("girando", i);
        }
    }

    IEnumerator girando(byte i)
    {
        bool aux = true;
        while (aux)
        {
            anim[i].gameObject.transform.parent.transform.Rotate(0, 1 * Time.deltaTime * -120, 0, Space.Self);
            if (anim[i].gameObject.transform.parent.transform.eulerAngles.y % 180 < 10)
            {
                if (!frente)
                {
                    anim[i].gameObject.transform.parent.transform.eulerAngles = Vector3.zero;
                    frente = !frente;
                }
                aux = false;
            }
            yield return new WaitForSeconds(0.01f);
        }
        if (anim[i].gameObject.transform.parent.transform.eulerAngles.y > 180 && anim[i].gameObject.transform.parent.transform.eulerAngles.y < 210)
        {
            anim[i].gameObject.transform.parent.transform.eulerAngles = anim[i].gameObject.transform.parent.transform.eulerAngles = Vector3.up * 180;
        }
        if (anim[i].gameObject.transform.parent.transform.eulerAngles.y > 0 && anim[i].gameObject.transform.parent.transform.eulerAngles.y < 30)
        {
            anim[i].gameObject.transform.parent.transform.eulerAngles = anim[i].gameObject.transform.parent.transform.eulerAngles = Vector3.up * 0;
        }
        GetComponent<Caminador>().walking = true;
        yield return null;
    }
}
