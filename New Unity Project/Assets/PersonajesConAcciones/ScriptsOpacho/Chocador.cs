﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chocador : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        other.transform.parent.GetComponent <Caminador>().walking = false;
        other.transform.parent.GetComponent<ManejaAnimacion>().halfTurn();
    }
}
