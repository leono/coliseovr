﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAnimation : MonoBehaviour {
    public bool startFight = false;
    Animator anim;
    // Use this for initialization
    void Start () {
        anim = transform.GetChild(0).GetComponent<Animator>();
		if(startFight)
        {
            changeToHit();
        }
	}
	
	public void changeToHit()
    {
        anim.SetTrigger("Pega");
    }

    public void changeToIdle()
    {
        anim.SetTrigger("Idle");
    }
}
