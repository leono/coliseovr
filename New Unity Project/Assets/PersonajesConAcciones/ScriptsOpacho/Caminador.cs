﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caminador : MonoBehaviour {
    public bool walking = true;
    public bool front = false;
    public GameObject Object1;
    public GameObject Object2;
    bool block = false;
    // Update is called once per frame
    void Update() {
        if(walking)
        {
            block = false;
            if(front)
            {
                Debug.Log("Adelante");
                transform.position = Vector3.MoveTowards(transform.position, Object2.transform.position, 7 * Time.deltaTime);
            } else
            {
                Debug.Log("Atras");
                transform.position = Vector3.MoveTowards(transform.position, Object1.transform.position, 7 * Time.deltaTime);
            }
        } else
        {
            if(!block)
            {
                block = true;
                front = !front;
            }
        }
    } 
}
