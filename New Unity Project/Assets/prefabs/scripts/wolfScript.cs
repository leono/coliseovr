﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wolfScript : MonoBehaviour
{
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void setToWalk()
    {
        animator.SetTrigger("idle");
        animator.SetTrigger("walk");

    }

    public void setToCreep()
    {
        animator.SetTrigger("idle");
        animator.SetTrigger("creep");

    }
    
    public void setToRun()
    {
        animator.SetTrigger("idle");
        animator.SetTrigger("run");

    }
    
    public void setToIdle()
    {
        animator.SetTrigger("idle");

    }
    
    public void setToSeat()
    {
        animator.SetTrigger("idle");
        animator.SetTrigger("seat");

    }
}
