﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cesarScript : MonoBehaviour
{
    public AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void glowIn()
    {
        //this.gameObject.gameObject.GetComponent<Behaviour>().enabled = true;
        
        Behaviour halo =(Behaviour) this.gameObject.GetComponent ("Halo");
        halo.enabled = true;
    }
    
    public void glowOut()
    {
        //this.gameObject.gameObject.GetComponent<Behaviour>().enabled = true;
        
        Behaviour halo =(Behaviour) this.gameObject.GetComponent ("Halo");
        halo.enabled = false;
    }

    public void playIntro()
    {
        audio.Play();
    }
}
