﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleport : MonoBehaviour
{
    public GameObject player;

    public void TeleportPlayer()
    {
        GameObject backdoor=GameObject.Find("teleport_back");
        GameObject frontdoor=GameObject.Find("teleport_front");
        
        float offset_z = 0.0f;
        float offset_x = 0.0f;
        float offset_y = 0.0f;

        Vector3  backdoor_position = new Vector3(backdoor.transform.position.x,backdoor.transform.position.y, backdoor.transform.position.z);
        Vector3  front_position = new Vector3(frontdoor.transform.position.x,frontdoor.transform.position.y, frontdoor.transform.position.z);
        Scene m_Scene = SceneManager.GetActiveScene();
        if (m_Scene.name.Equals("scene1"))
        {

            if (backdoor_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
            {
                SceneManager.LoadScene("scene2");
            }
            else
            {
                if (backdoor_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
                {
                    Debug.Log("Backdoor position");
                    offset_x = -55.0f;
                    offset_y = -1.5f;
                }
                else if(front_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
                {
                    Debug.Log("Frontdoor position");
                    offset_x = 55.0f;
                    offset_y = -1.5f;
                }
                player.transform.position = new Vector3(transform.position.x+offset_x,transform.position.y +1.5f + offset_y, transform.position.z+offset_z);
                
            }

        }
        else if (m_Scene.name.Equals("scene2"))
        {
            if (front_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
            {
                SceneManager.LoadScene("scene1");
            }
            else
            {
                if (backdoor_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
                {
                    Debug.Log("Backdoor position");
                    offset_x = -55.0f;
                    offset_y = -1.5f;
                }
                else if(front_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
                {
                    Debug.Log("Frontdoor position");
                    offset_x = 55.0f;
                    offset_y = -1.5f;
                }
                player.transform.position = new Vector3(transform.position.x+offset_x,transform.position.y +1.5f + offset_y, transform.position.z+offset_z);
            }
            
        }

        /*if (backdoor_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
        {
            Debug.Log("Backdoor position");
            offset_x = -55.0f;
            offset_y = -1.5f;
        }
        else if(front_position.Equals(new Vector3(transform.position.x, transform.position.y, transform.position.z)))
        {
            Debug.Log("Frontdoor position");
            offset_x = 55.0f;
            offset_y = -1.5f;
        }*/

        //player.transform.position = new Vector3(transform.position.x+offset_x,transform.position.y +1.5f + offset_y, transform.position.z+offset_z);
    }
    
    
}
