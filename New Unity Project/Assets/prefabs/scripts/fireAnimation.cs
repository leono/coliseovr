﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireAnimation : MonoBehaviour
{
    public AudioSource audiofx;
    public AudioSource voicefx;

    public AudioClip audioClip1;
    public AudioClip audioClip2;
    
    public Animator animator;
    public Animator animatorGladiator;

    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Awake() {
        
        //animator.SetTrigger("raisefighters");
        //audiofx.Play();
        //voicefx.Play();
        //Debug.Log("fire animations:"+gameObject.name);
        //GameObject[] gos = GameObject.FindGameObjectsWithTag("Teleport");
        //GameObject closest = GetClosestEnemy(gos);
        //Debug.Log("closest:"+closest.name);
        //closest.gameObject.GetComponent<Renderer>().enabled = false;
        //closest.gameObject.GetComponent<Collider>().enabled = false;
        //player.transform.position = new Vector3(player.transform.position.x,player.transform.position.y -1.5f , player.transform.position.z);  
    }
    
    GameObject GetClosestEnemy(GameObject[] enemies)
    {
        GameObject tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;
        foreach (GameObject t in enemies)
        {
            float dist = Vector3.Distance(t.transform.position, currentPos);
            if (dist < minDist)
            {
                tMin = t;
                minDist = dist;
            }
        }
        return tMin;
    }
}
