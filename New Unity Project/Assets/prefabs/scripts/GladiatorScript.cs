﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GladiatorScript : MonoBehaviour
{
    public Animator animator;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setToAlert()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("alert");

    }

    public void setToFightIdle()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("fight idle");

    }
    
    public void setToDye()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("dye");

    }
    
    public void setToPoolJump()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("pool jump");

    }
    
    public void setToAttack()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("attack");

    }
    
    public void setToDash()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("dash");

    }
    
    public void setToRun()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("run");

    }
    
    public void setToIdle()
    {
        animator.SetTrigger("empty");
        animator.SetTrigger("idle");

    }

}
