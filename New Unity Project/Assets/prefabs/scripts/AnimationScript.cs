﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimationScript : MonoBehaviour
{    
    public Animator animator;

    public Animator arenaAnimator;
    public Animator arenaFightAnimator;
    private bool status = true;
    private bool mechanics = true;
    private bool flood = true;
    private bool fight = true;
    private bool velarium = true;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void raiseArena()
    {
        if (status)
        {
            //animator.SetTrigger("Unflood");
            //animator.SetTrigger("EndMechanisc");
            //arenaFightAnimator.SetTrigger("lowerfighters");
            //fight = true;
            
            animator.SetTrigger("show");
            status = false;
        }
        else
        {
            animator.SetTrigger("unshow");
            status = true;
        }

    }

    public void startArenaMechaniscs()
    {
        Debug.Log("startArena:" + mechanics);
        if (mechanics)
        { 
            //animator.SetTrigger("Unflood");
            //animator.SetTrigger("unshow");
            animator.SetTrigger("StartMechanisc");
            mechanics = false;
        }
        else
        {
            Debug.Log("Down" );
            animator.SetTrigger("EndMechanisc");
            mechanics = true;
        }
    }
    
    public void startFlood()
    {
        Debug.Log("startFlood:" + mechanics);
        if (flood)
        { 
            //animator.SetTrigger("EndMechanisc");
            //animator.SetTrigger("unshow");
            animator.SetTrigger("Flood");
            flood = false;
        }
        else
        {
            Debug.Log("Down" );
            animator.SetTrigger("Unflood");
            flood = true;
        }
    }
    public void startGladiatorsFight()
    {
        Debug.Log("startGladiator:" + mechanics);
        if (fight)
        { 
            //animator.SetTrigger("EndMechanisc");
            //animator.SetTrigger("unshow");
            
            //arenaAnimator.SetTrigger("unshow");
            //status = true;
            //WaitForSeconds wait= new WaitForSeconds(3);
            animator.SetTrigger("raisefighters");
            fight = false;
        }
        else
        {
            Debug.Log("Down" );
            animator.SetTrigger("lowerfighters");
            fight = true;
        }
    }
    
    public void startVelarium()
    {
        Debug.Log("velarium:" + velarium);
        if (velarium)
        { 
            //animator.SetTrigger("EndMechanisc");
            //animator.SetTrigger("unshow");
            animator.SetTrigger("solariumOn");
            velarium = false;
        }
        else
        {
            Debug.Log("Down" );
            animator.SetTrigger("solariumOff");
            velarium = true;
        }
    }

    public void changeScene(string sceneName)
    {
        GameObject[] gameObjects;
        gameObjects = GameObject.FindGameObjectsWithTag("Player");

        if (gameObjects.Length == 0)
        {
            DontDestroyOnLoad(gameObjects[0]);
        }
        gameObjects = GameObject.FindGameObjectsWithTag("MainCamera");

        if (gameObjects.Length == 0)
        {
            DontDestroyOnLoad(gameObjects[0]);
        }
        SceneManager.LoadScene(sceneName);
        //animator.SetTrigger("Flood");
        //flood = false;
    }
    

}
