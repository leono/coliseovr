﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRGaze : MonoBehaviour
{
    public Image imgGaze;
    public float totalTime = 1;
    bool gvrStatus;

    float gvrTimer;

    public int distanceOfRay = 50;
    private RaycastHit _hit;

    private GameObject hit_object;
    public Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {   Debug.Log("gvrStatus: "+gvrStatus);
        if (gvrStatus)
        {
            gvrTimer += Time.deltaTime;
            //Debug.Log("gvrTimer step: "+gvrTimer);
            imgGaze.fillAmount = gvrTimer / 1;
            //Debug.Log("gvrTimer total: "+ gvrTimer +"/" + 1 +"="+imgGaze.fillAmount);
        }
        
        //RaycastHit2D hit = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(touch.position).x, Camera.main.ScreenToWorldPoint(touch.position).y), Vector2.zero, 0f);
        //if (hit.rigidbody != null)

        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        //Debug.Log("Ray cast:"+Physics.Raycast(ray, out _hit,  Mathf.Infinity)+" - Distance:"+ray.ToString());
        if (Physics.Raycast(ray, out _hit, Mathf.Infinity))
        {


            if (_hit.transform.CompareTag("Gear"))
            {
                //Debug.Log("Input touch:"+Input.touchCount);
                if (Input.touchCount > 0 && Input.GetTouch(0).phase== TouchPhase.Stationary)
                {
                    _hit.transform.gameObject.transform.Rotate(Vector3.up * 90 * Time.deltaTime);    
                }
                if (Input.touchCount > 0 && Input.GetTouch(0).phase== TouchPhase.Ended)
                {
                    //_hit.transform.gameObject.transform.Rotate(Vector3.up * 90 * Time.deltaTime);    
                }
  
            }

            Debug.Log("imgGaze.fillAmount == 1"+(imgGaze.fillAmount == 1));
            if (imgGaze.fillAmount == 1 && _hit.transform.CompareTag("Teleport"))
            {    
                
                
                //Debug.Log("executes method teleport");
                hit_object = _hit.transform.gameObject;//_hit.transform.gameObject.GetComponent<Teleport>().TeleportPlayer();
                animator.SetTrigger("fadeout");
                hit_object.GetComponent<Teleport>().TeleportPlayer();
                
                GameObject[] gos = GameObject.FindGameObjectsWithTag("Teleport");
                foreach (GameObject go in gos) {
                    go.GetComponent<Renderer>().enabled = true;
                    go.GetComponent<Collider>().enabled = true;
                    
                }

                _hit.transform.gameObject.GetComponent<Renderer>().enabled = false;
                _hit.transform.gameObject.GetComponent<Collider>().enabled = false;
                gvrStatus = false;
                gvrTimer = 0;
                imgGaze.fillAmount = 0;
                animator.SetTrigger("fadein");
            }
        }
    }

    public void GVROn()
    {
        gvrStatus = true;
    }

    public void GVROff()
    {
        gvrStatus = false;
        gvrTimer = 0;
        imgGaze.fillAmount = 0;
    }

    public void onFadeCompete()
    {
        //hit_object.GetComponent<Teleport>().TeleportPlayer();
    }
}
