﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartInStatus : MonoBehaviour

{
    public string startInStatus;
    public Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {    
        Debug.Log("Animator"+animator.gameObject.name+" set the animation:"+ startInStatus);
        animator.SetTrigger(startInStatus);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
