﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioeffects : MonoBehaviour
{
    public AudioSource audiofx;
    public AudioSource voicefx;
    public AudioSource musicfx;

    public AudioClip audioClip1;
    public AudioClip audioClip2;
    private bool status = true;
    private bool mechanics = true;
    private bool flood = true;
    private bool fight = true;
    private bool velarium = true;
    private AudioSource[] allAudioSources;
    // Start is called before the first frame update
    void Start()
    {
        voicefx = GetComponents<AudioSource>()[0];
        audiofx = GetComponents<AudioSource>()[1];
        musicfx = GetComponents<AudioSource>()[2];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void playEffectArena()
    {
        Debug.Log("Arena Status:"+status);
        playEffectSpecific(ref status);
    }
    
    public void playEffectMechanisc()
    {
        playEffectSpecific(ref mechanics);
    }
    
    public void playEffectFlood()
    {
        Debug.Log("Flood Status:"+flood);
        playEffectSpecific(ref flood);
    }
    
    public void playEffectFight()
    {
        playEffectSpecific(ref fight);
    }
    
    public void playEffectVelarium()
    {
        playEffectSpecific(ref velarium);
    }
    
    public void playEffectSpecific(ref bool flag)
        {
            StopAllAudio();
            //if (!flag)
            //{
             //   StopAllAudio();
            //}

            musicfx.Play();
            audiofx.Play();
            if (flag)
            { 
                voicefx.Play();
                flag = false;
            }
            else
            {
                flag = true;
            }
        }
        
    
    
    void StopAllAudio() {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach( AudioSource audioS in allAudioSources) {
            audioS.Stop();
        }
    }
    
    public void playEffectvoiceOnly()
    {
        voicefx.Play();
    }
}
